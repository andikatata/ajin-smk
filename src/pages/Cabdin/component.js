import React from 'react';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import FormatList from '@material-ui/icons/FormatListBulletedOutlined';
import Navbar from '../../components/elements/Navbar';
import SideBar from '../../components/elements/SideBar';
import Avatar from '@material-ui/core/Avatar';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import FiberNew from '@material-ui/icons/FiberNew';
import Sync from '@material-ui/icons/Sync';
import DoneAll from '@material-ui/icons/DoneAll';
import Divider from '@material-ui/core/Divider';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Charts from 'react-google-charts';
import Button from '@material-ui/core/Button';
import Cached from '@material-ui/icons/Cached';
import classNames from 'classnames';

const options = {
  pieHole: 0.4,
  is3D: false,
  legend: 'none',
  pieSliceText: 'label',
};
const data = [
  ['Tanggal', 'Pendirian Sekolah', 'Ijin Perpanjangan', 'Penambahan Proli', 'Pengubahan Proli'],
  ['2018-01', 5, 10, 2, 20],
  ['2018-02', 10, 2, 5, 5],
  ['2018-03', 3, 6, 1, 10],
  ['2018-04', 6, 2, 10, 2],
  // ['2018-05', 5, 2, 0, 1],
  // ['2018-06', 2, 8, 15, 3],
  // ['2018-07', 7, 1, 12, 3],
  // ['2018-08', 5, 1, 8, 1],
  // ['2018-09', 2, 3, 1, 1],
  ['2018-10', 1, 9, 6, 12],
  ['2018-11', 4, 11, 15, 1],
  ['2018-12', 19, 15, 11, 1],
];
export default class Component extends React.Component {
  state = {
    open: false,
    activeStep: 1,
  };
  handleClick = () => {
    this.setState(state => ({ open: !state.open }));
  };


  render() {
    const { classes, isLoading } = this.props;

    if (isLoading) {
      return (
        <Typography variant="display2">Loading...</Typography>
      );
    } else {
      return (
        <section className={classes.container}>
          <div className={classes.root}>
            <CssBaseline />
            <Navbar />
            <SideBar />
            {/* MAIN */}
            <main className={classes.content}>
              <div className={classes.toolbar} />
              <Typography className={classes.judul}>Dashboard</Typography>
              <Grid container spacing={24}>
                <Grid item xs={3}>
                  <Paper>
                    <List>
                      <ListItem>
                        <Avatar>
                          <FormatList />
                        </Avatar>
                        <ListItemText style={{ color: 'white', }} primary="Pendirian Sekolah" secondary="50" />
                      </ListItem>
                    </List>
                  </Paper>
                </Grid>
                <Grid item xs={3}>
                  <Paper>
                    <List>
                      <ListItem>
                        <Avatar>
                          <FiberNew />
                        </Avatar>
                        <ListItemText primary="Perpanjangan Ijin" secondary="02" />
                      </ListItem>
                    </List>
                  </Paper>
                </Grid>
                <Grid item xs={3}>
                  <Paper>
                    <List>
                      <ListItem className={classes.pp}>
                        <Avatar>
                          <Sync />
                        </Avatar>
                        <ListItemText primary="Penambahan Proli" secondary="15" />
                      </ListItem>
                    </List>
                  </Paper>
                </Grid>
                <Grid item xs={3}>
                  <Paper>
                    <List>
                      <ListItem className={classes.pp}>
                        <Avatar>
                          <DoneAll />
                        </Avatar>
                        <ListItemText primary="Pengubahan Proli" secondary="37" />
                      </ListItem>
                    </List>
                  </Paper>
                </Grid>

                <Grid item xs={12}>
                  <Paper>
                    <div >
                      <Charts
                        chartType="LineChart"
                        loader={<div>Loading Chart</div>}
                        width="170vh"
                        height="400px"
                        margins={{ left: '100px' }}
                        data={data}
                        options={{
                          curveType: 'function',
                          legend: { position: 'top' },
                        }}
                      />
                    </div>
                  </Paper>
                </Grid>

                <Grid item xs={4}>
                  <Paper>
                    <Typography className={classes.Header3} align="Center">PERFORMANSI PENGERJAAN</Typography>
                    <Divider />
                    <div>
                      <Charts
                        chartType="PieChart"
                        width="100%"
                        height="300px"
                        loader={<div>Loading Chart</div>}
                        data={[
                          ['Status', 'Persen'],
                          ['>10 H', 11],
                          ['5-10 H', 5],
                          ['<5 H', 2],]}
                        options={options}
                      />
                    </div>
                  </Paper>
                </Grid>

                <Grid item xs={8}>
                  <Paper style={{ padding: '20px' }} className={classes.paper}>
                    <Typography className={classes.Header} align="Left" style={{ fontSize: '18px', }}>DAFTAR PERIJINAN BARU</Typography>
                    <Divider />
                    <Table className={classes.table}>
                      <TableHead>
                        <TableRow>
                          <TableCell style={{ padding: 10, fontSize: '14px' }}>#</TableCell>
                          <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">Nama Sekolah/Yayasan</TableCell>
                          <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">Jenis Perijinan</TableCell>
                        </TableRow>
                      </TableHead>

                      <TableBody>
                        <TableRow>
                          <TableCell className={classes.kolom} component="th" scope="row">1</TableCell>
                          <TableCell className={classes.kolom1} style={{ minWidth: '200px' }} align="left">Yayasan Pendidikan Telkom</TableCell>
                          <TableCell style={{ padding: 10 }} >
                            <span style={{ backgroundColor: '#E91E63', color: '#FFFFFF', padding: '.2em .6em .3em', fontSize: '90%' }}>
                              Pendirian Sekolah
                            </span>
                          </TableCell>
                          <TableCell style={{ padding: 10, minWidth: '120px' }} align="left">
                            <Button variant="contained" size="small" className={classes.buttonup}>
                              <Cached className={classNames(
                                classes.leftIcon,
                                classes.iconSmall,
                              )} />
                              PROSES
                            </Button>
                          </TableCell>
                        </TableRow>
                      </TableBody>

                      <TableBody>
                        <TableRow>
                          <TableCell className={classes.kolom} component="th" scope="row">2</TableCell>
                          <TableCell className={classes.kolom1} align="left">SMK Telkom Malang</TableCell>
                          <TableCell style={{ padding: 10 }} >
                            <span style={{ backgroundColor: '#03A9F4', color: '#FFFFFF', padding: '.2em .6em .3em', fontSize: '90%' }}>
                              Perpanjangan Ijin
                            </span>
                          </TableCell>
                          <TableCell style={{ padding: 10, minWidth: '100px' }} align="left">
                            <Button variant="contained" size="small" className={classes.buttonup}>
                              <Cached className={classNames(
                                classes.leftIcon,
                                classes.iconSmall,
                              )} />
                              PROSES
                            </Button>
                          </TableCell>
                        </TableRow>
                      </TableBody>

                      <TableBody>
                        <TableRow>
                          <TableCell className={classes.kolom} component="th" scope="row">3</TableCell>
                          <TableCell className={classes.kolom1} align="left">SMKN 1 Surabaya</TableCell>
                          <TableCell style={{ padding: 10 }} >
                            <span style={{ backgroundColor: '#4CAF50', color: '#FFFFFF', padding: '.2em .6em .3em', fontSize: '90%' }}>
                              Program Keahlian Baru
                            </span>
                          </TableCell>
                          <TableCell style={{ padding: 10, minWidth: '100px' }} align="left">
                            <Button variant="contained" size="small" className={classes.buttonup}>
                              <Cached className={classNames(
                                classes.leftIcon,
                                classes.iconSmall,
                              )} />
                              PROSES
                            </Button>
                          </TableCell>
                        </TableRow>
                      </TableBody>

                      <TableBody>
                        <TableRow>
                          <TableCell className={classes.kolom} component="th" scope="row">4</TableCell>
                          <TableCell className={classes.kolom1} align="left">SMKN 2 Gresik</TableCell>
                          <TableCell style={{ padding: 10 }}>
                            <span style={{ backgroundColor: '#FF9800', color: '#FFFFFF', padding: '.2em .6em .3em', fontSize: '90%' }}>
                              Pengubahan Program Keahlian
                            </span>
                          </TableCell>
                          <TableCell style={{ padding: 10, minWidth: '100px' }} align="left">
                            <Button variant="contained" size="small" className={classes.buttonup}>
                              <Cached className={classNames(
                                classes.leftIcon,
                                classes.iconSmall,
                              )} />
                              PROSES
                            </Button>
                          </TableCell>
                        </TableRow>
                      </TableBody>

                      <TableBody>
                        <TableRow>
                          <TableCell className={classes.kolom} component="th" scope="row">5</TableCell>
                          <TableCell className={classes.kolom1} align="left">SMK Buduran Sidoarjo</TableCell>
                          <TableCell style={{ padding: 10 }}>
                            <span style={{ backgroundColor: '#4CAF50', color: '#FFFFFF', padding: '.2em .6em .3em', fontSize: '90%' }}>
                              Program Keahlian Baru
                            </span>
                          </TableCell>
                          <TableCell style={{ padding: 10, minWidth: '100px' }} align="left">
                            <Button variant="contained" size="small" className={classes.buttonup}>
                              <Cached className={classNames(
                                classes.leftIcon,
                                classes.iconSmall,
                              )} />
                              PROSES
                            </Button>
                          </TableCell>
                        </TableRow>
                      </TableBody>
                    </Table>
                  </Paper>
                </Grid>
              </Grid>

            </main>
          </div>
        </section>
      );
    }
  }
}

Component.propTypes = {
  classes: PropTypes.object,
  actions: PropTypes.object,
  isLoading: PropTypes.bool,
  data: PropTypes.array
};