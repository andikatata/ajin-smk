
const drawerWidth = 300;
const styles = theme => ({
  container: {
    textAlign: 'center'
  },
  logo: {
    maxWidth: '100%'
  },
  routeExample: {
    marginBottom: '20px'
  },
  avatar:{
    width: '4vw',
    height: '4vw',
    marginTop: '-5vh'
  },
  root: {
    display: 'flex',
  },
  body:{
    padding: '10px 20px',
    margin: '20px 0 20px 10px',
    borderLeft:'5px solid #eee',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
  ppk: {
    paddingRight: 0,
  },
  paper: {
    padding: theme.spacing.unit * 1,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  linearColorPrimary1: {
    backgroundColor: '#efefef',
  },
  linearBarColorPrimary1: {
    backgroundColor: '#03A9F4',
  },
  linearColorPrimary2: {
    backgroundColor: 'efefef',
  },
  linearBarColorPrimary2: {
    backgroundColor: '#4CAF50'
  },
  linearColorPrimary3: {
    backgroundColor: 'efefef',
  },
  linearBarColorPrimary3: {
    backgroundColor: '#FF9800',
  },
  main: {
    paddingTop: '5px',
    paddingBottom: '5px',
    marginBottom: '10px',
    background : '#efefef',
  },
  image:{
    display: 'block',
    height: '135px',
    padding:'7vh 15px 12px 15px',
    backgroundImage: 'url(../../../assets/schoolvector.jpg)',
  },
  greeting:{
    color: 'white'
  },
  judul:{
    fontSize:'20px',
    marginBottom:'5px',
    float: 'left',
  },
  pp: {
    paddingRight: 0,
  },
  Header: {
    padding: 10,
    fontSize:'18px',
  },
  Header3: {
    padding: '10px 15px 10px 15px',
    fontSize:'18px',
  },
  icon: {
    backgroundColor: '#E91E63',
  },
  kolom: {
    padding: 10, 
    fontSize:'14px',
  },

  toolbar: theme.mixins.toolbar,
});

export default styles;