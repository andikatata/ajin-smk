
import React from 'react';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import FormatList from '@material-ui/icons/FormatListBulletedOutlined';
import Navbar from '../../components/elements/Navbar';
import SideBar from '../../components/elements/SideBar';
import Avatar from '@material-ui/core/Avatar';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import FiberNew from '@material-ui/icons/FiberNew';
import Sync from '@material-ui/icons/Sync';
import DoneAll from '@material-ui/icons/DoneAll';
import Divider from '@material-ui/core/Divider';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import LinearProgress from '@material-ui/core/LinearProgress';
import Charts from 'react-google-charts';

function getSteps() {
  return [
    'Pengajuan',
    'Verifikasi Cabdin',
    'Verifikasi P2T',
    'Verifikasi Disdik',
    'Penerbitan'];
}
const options = {
  pieHole: 0.4,
  is3D: false,
  legend: 'none',
  pieSliceText: 'label',
};
export default class Component extends React.Component {
  state = {
    open: false,
    activeStep: 1,
  };
  handleClick = () => {
    this.setState(state => ({ open: !state.open }));
  };


  render() {
    const { classes, isLoading } = this.props;
    const steps = getSteps();
    const { activeStep } = this.state;

    if (isLoading) {
      return (
        <Typography variant="display2">Loading...</Typography>
      );
    } else {
      return (
        <section className={classes.container}>
          <div className={classes.root}>
            <CssBaseline />
            <Navbar/>
            <SideBar/>
            {/* MAIN */}
            <main className={classes.content}>
              <div className={classes.toolbar} />
              <Typography className={classes.judul}>Dashboard</Typography>
              <Grid container spacing={24}>
                <Grid item xs={3}>
                  <Paper>
                    <List>
                      <ListItem>
                        <Avatar>
                          <FormatList />
                        </Avatar>
                        <ListItemText style={{ color: 'white', }} primary="Total Perijinan" secondary="50" />
                      </ListItem>
                    </List>
                  </Paper>
                </Grid>
                <Grid item xs={3}>
                  <Paper>
                    <List>
                      <ListItem>
                        <Avatar>
                          <FiberNew />
                        </Avatar>
                        <ListItemText primary="Perijinan Baru" secondary="02" />
                      </ListItem>
                    </List>
                  </Paper>
                </Grid>
                <Grid item xs={3}>
                  <Paper>
                    <List>
                      <ListItem className={classes.pp}>
                        <Avatar>
                          <Sync />
                        </Avatar>
                        <ListItemText primary="Proses Perijinan" secondary="15" />
                      </ListItem>
                    </List>
                  </Paper>
                </Grid>
                <Grid item xs={3}>
                  <Paper>
                    <List>
                      <ListItem className={classes.pp}>
                        <Avatar>
                          <DoneAll />
                        </Avatar>
                        <ListItemText primary="Perijinan Selesai" secondary="37" />
                      </ListItem>
                    </List>
                  </Paper>
                </Grid>

                <Grid item xs={12}>
                  <Paper style={{ padding: '20px' }}>
                    <Typography className={classes.Header} align="Left">PROGRES TERKINI</Typography>
                    <Divider />
                    <div className={classes.body}>
                      <Typography align="left" style={{ fontSize: '18px', }}>Pengajuan pendirian SMK Telkom Sidoarjo</Typography>
                    </div>
                    <div>
                      <Stepper activeStep={activeStep} >
                        {steps.map(label => (
                          <Step key={label}>
                            <StepLabel>{label}</StepLabel>
                          </Step>
                        ))}
                      </Stepper>
                    </div>
                  </Paper>
                </Grid>

                <Grid item xs={8}>
                  <Paper style={{ padding: '20px' }} className={classes.paper}>
                    <Typography className={classes.Header} align="Left" style={{ fontSize: '18px', }}>DAFTAR PROGRES PERIJINAN</Typography>
                    <Divider />
                    <Table className={classes.table}>
                      <TableHead>
                        <TableRow>
                          <TableCell style={{ padding: 10, fontSize: '14px' }}>#</TableCell>
                          <TableCell style={{ padding: 10, minWidth: 120, fontSize: '14px' }} align="center">Nama Perijinan</TableCell>
                          <TableCell style={{ padding: 10, fontSize: '14px' }} align="center">Jenis Perijinan</TableCell>
                          <TableCell style={{ padding: 10, minWidth: 120, fontSize: '14px' }} align="center">Status</TableCell>
                          <TableCell style={{ padding: 10, fontSize: '14px' }} align="center">Update Terakhir</TableCell>
                          <TableCell style={{ padding: 10, fontSize: '14px' }} align="center">Progress</TableCell>
                        </TableRow>
                      </TableHead>

                      <TableBody>
                        <TableRow>
                          <TableCell className={classes.kolom} component="th" scope="row">1</TableCell>
                          <TableCell className={classes.kolom} align="left">Pendirian SMK Telkom Sidoarjo</TableCell>
                          <TableCell className={classes.kolom} align="left">Pendirian Sekolah Baru</TableCell>
                          <TableCell style={{ padding: 10 }} align="center" >
                            <span style={{ backgroundColor: '#03A9F4', color: '#FFFFFF', padding: '.2em .6em .3em', fontSize: '75%' }}>
                              Diterima Cabdin
                            </span>
                          </TableCell>
                          <TableCell className={classes.kolom} align="right">21 Jan 2018</TableCell>
                          <TableCell style={{ padding: 10, }} align="right">
                            <LinearProgress
                              classes={{
                                colorPrimary: classes.linearColorPrimary1,
                                barColorPrimary: classes.linearBarColorPrimary1,
                              }}
                              style={{ height: '8px' }} variant="determinate" value="10" />
                          </TableCell>
                        </TableRow>
                      </TableBody>

                      <TableBody>
                        <TableRow>
                          <TableCell className={classes.kolom} component="th" scope="row">2</TableCell>
                          <TableCell className={classes.kolom} align="left">Perpanjangan Ijin SMK Telkom Malang</TableCell>
                          <TableCell className={classes.kolom} align="left">Perpanjangan Ijin</TableCell>
                          <TableCell style={{ padding: 10, }} align="center" >
                            <span style={{ backgroundColor: '#03A9F4', color: '#FFFFFF', padding: '.2em .6em .3em', fontSize: '75%' }}>
                              Diterima Cabdin
                            </span>
                          </TableCell>
                          <TableCell className={classes.kolom} align="right">20 Jan 2018</TableCell>
                          <TableCell style={{ padding: 10, }} align="right">
                            <LinearProgress
                              classes={{
                                colorPrimary: classes.linearColorPrimary1,
                                barColorPrimary: classes.linearBarColorPrimary1,
                              }}
                              style={{ height: '8px', }} variant="determinate" value="10" />
                          </TableCell>
                        </TableRow>
                      </TableBody>

                      <TableBody>
                        <TableRow>
                          <TableCell className={classes.kolom} component="th" scope="row">3</TableCell>
                          <TableCell className={classes.kolom} align="left">Penambahan Proli RPL di SMK Telkom Malang</TableCell>
                          <TableCell className={classes.kolom} align="left">Penambahan Program Keahlian</TableCell>
                          <TableCell style={{ padding: 10, }} align="center" >
                            <span style={{ backgroundColor: '#4CAF50', color: '#FFFFFF', padding: '.2em .6em .3em', fontSize: '75%' }}>
                              Proses kirim ke P2T
                            </span>
                          </TableCell>
                          <TableCell className={classes.kolom} align="right">30 Jan 2018</TableCell>
                          <TableCell style={{ padding: 10, }} align="right">
                            <LinearProgress
                              classes={{
                                colorPrimary: classes.linearColorPrimary2,
                                barColorPrimary: classes.linearBarColorPrimary2,
                              }}
                              style={{ height: '8px', }} variant="determinate" value="40" />
                          </TableCell>
                        </TableRow>
                      </TableBody>

                      <TableBody>
                        <TableRow>
                          <TableCell className={classes.kolom} component="th" scope="row">4</TableCell>
                          <TableCell className={classes.kolom} align="left">Pengubahan Proli TKJ ke TJA SMK Telkom Malang</TableCell>
                          <TableCell className={classes.kolom} align="left">Pengubahan Program Keahlian</TableCell>
                          <TableCell style={{ padding: 10, }} align="center">
                            <span style={{ backgroundColor: '#4CAF50', color: '#FFFFFF', padding: '.2em .6em .3em', fontSize: '75%' }}>
                              Verifikasi Disdik
                            </span>
                          </TableCell>
                          <TableCell className={classes.kolom} align="right">3 Feb 2018</TableCell>
                          <TableCell style={{ padding: 10, }} align="right">
                            <LinearProgress
                              classes={{
                                colorPrimary: classes.linearColorPrimary2,
                                barColorPrimary: classes.linearBarColorPrimary2,
                              }}
                              style={{ height: '8px', }} variant="determinate" value="70" />
                          </TableCell>
                        </TableRow>
                      </TableBody>

                      <TableBody>
                        <TableRow>
                          <TableCell className={classes.kolom} component="th" scope="row">5</TableCell>
                          <TableCell className={classes.kolom} align="left">Penambahan Proli Multimedia di SMK Telkom Malang</TableCell>
                          <TableCell className={classes.kolom} align="left">Penambahan Program Keahlian</TableCell>
                          <TableCell style={{ padding: 10, }} align="center">
                            <span style={{ backgroundColor: '#FF9800', color: '#FFFFFF', padding: '.2em .6em .3em', fontSize: '75%' }}>
                              Penerbitan Sertifikat
                            </span>
                          </TableCell>
                          <TableCell className={classes.kolom} align="right">31 Jan 2018</TableCell>
                          <TableCell style={{ padding: 10, }} align="right">
                            <LinearProgress
                              classes={{
                                colorPrimary: classes.linearColorPrimary3,
                                barColorPrimary: classes.linearBarColorPrimary3,
                              }}
                              style={{ height: '8px', }} variant="determinate" value="100" />
                          </TableCell>
                        </TableRow>
                      </TableBody>
                    </Table>
                  </Paper>
                </Grid>

                <Grid item xs={4}>
                  <Paper>
                    <Typography className={classes.Header3} align="Center">PROSENTASE PROGRES PERIJINAN</Typography>
                    <Divider />
                    <div>
                      <Charts
                        chartType="PieChart"
                        width="100%"
                        height="300px"
                        loader={<div>Loading Chart</div>}
                        data={[
                          ['Status', 'Persen'],
                          ['Selesai', 11],
                          ['Proses', 5],
                          ['Baru', 2],]}
                        options={options}
                      />
                    </div>
                  </Paper>
                </Grid>
              </Grid>

            </main>
          </div>
        </section>
      );
    }
  }
}

Component.propTypes = {
  classes: PropTypes.object,
  actions: PropTypes.object,
  isLoading: PropTypes.bool,
  data: PropTypes.array
};
