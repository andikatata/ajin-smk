import React from 'react';
import PropTypes from 'prop-types';
import { CssBaseline, Typography, Paper, Grid, Table, TableBody, TableCell, TableFooter, TablePagination, TableRow, TableHead, IconButton, withStyles, Button } from '@material-ui/core';
import Navbar from '../../components/elements/Navbar';
import SideBar from '../../components/elements/SideBar';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import PrintIcon from '@material-ui/icons/Print';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Cached from '@material-ui/icons/Cached';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Input from '@material-ui/core/Input';
import InsertIcon from '@material-ui/icons/InsertDriveFile';
import CheckIcon from '@material-ui/icons/Check';

const actionsStyles = theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit * 2.5,
  },

});

class TablePaginationActions extends React.Component {
  handleFirstPageButtonClick = event => {
    this.props.onChangePage(event, 0);
  };

  handleBackButtonClick = event => {
    this.props.onChangePage(event, this.props.page - 1);
  };

  handleNextButtonClick = event => {
    this.props.onChangePage(event, this.props.page + 1);
  };

  handleLastPageButtonClick = event => {
    this.props.onChangePage(
      event,
      Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1),
    );
  };

  render() {
    const { classes, count, page, rowsPerPage, theme } = this.props;

    return (
      <div className={classes.root}>
        <IconButton
          onClick={this.handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="First Page"
        >
          {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton
          onClick={this.handleBackButtonClick}
          disabled={page === 0}
          aria-label="Previous Page"
        >
          {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
        </IconButton>
        <IconButton
          onClick={this.handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Next Page"
        >
          {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
        </IconButton>
        <IconButton
          onClick={this.handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Last Page"
        >
          {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </div>
    );
  }
}

TablePaginationActions.propTypes = {
  classes: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  theme: PropTypes.object.isRequired,
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, { withTheme: true })(
  TablePaginationActions,
);

let id = 0;
function createData(nopengajuan, namayayasan, tanggalpengajuan, status) {
  id += 1;
  return { id, nopengajuan, namayayasan, tanggalpengajuan, status };
}
const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: '#efefef',
    color: theme.palette.common.black,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);


export default class Component extends React.Component {
  state = {
    open: false,
    activeStep: 1,
    scroll: 'paper',
    fullWidth: true,
    maxWidth: 'md',
    rows: [
      createData('17/BARU/Jatim/II/2018', 'Yayasan Pendidikan Telkom', '21 Jan 2018', 'Baru'),
      createData('17/BARU/Jatim/II/2018', 'Yayasan Pendidikan Telkom', '21 Jan 2018', 'Dalam Proses'),
      createData('17/BARU/Jatim/II/2018', 'Yayasan Pendidikan Telkom', '21 Jan 2018', 'Selesai'),
      createData('17/BARU/Jatim/II/2018', 'Yayasan Pendidikan Telkom', '21 Jan 2018', 'Dalam Proses'),
      createData('17/BARU/Jatim/II/2018', 'Yayasan Pendidikan Telkom', '21 Jan 2018', 'Selesai'),

    ].sort((a, b) => (a.calories < b.calories ? -1 : 1)),
    page: 0,
    rowsPerPage: 5,
  };
  handleClick = () => {
    this.setState(state => ({ open: !state.open }));
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ page: 0, rowsPerPage: event.target.value });
  };
  handleClickOpen = scroll => () => {
    this.setState({ open: true, scroll });
  };

  handleClose = () => {
    this.setState({ open: false });
  };


  render() {
    const { classes, isLoading } = this.props;
    const { rows, rowsPerPage, page } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

    if (isLoading) {
      return (
        <Typography variant="display2">Loading...</Typography>
      );
    } else {
      return (
        <section className={classes.container}>
          <div className={classes.root}>
            <CssBaseline />
            <Navbar />
            <SideBar />

            {/* MAIN */}
            <main className={classes.content}>
              <div className={classes.toolbar} />
              <Typography className={classes.judul}>PERIJINAN PENDIRIAN SEKOLAH BARU</Typography>
              <Grid container spacing={24}>
                <Grid item xs={12}>
                  <Paper style={{ padding: '20px' }}>
                    <div className={classes.dtbuttons} align="left">
                      <Button variant="contained" className={classes.button1}>Copy</Button>
                      <Button variant="contained" className={classes.button1}>CSV</Button>
                      <Button variant="contained" className={classes.button1}>Excel</Button>
                      <Button variant="contained" className={classes.button1}>PDF</Button>
                      <Button variant="contained" className={classes.button1}>Print</Button>
                    </div>
                    <div align="right" style={{ marginTop: '-35px' }}>
                      <label>
                        Search :
                        <Input
                          className={classes.input}
                          inputProps={{
                            'aria-label': 'Description',
                          }}
                        />
                      </label>
                    </div>

                    {/* MODAL */}
                    <div className="modal">
                      <Dialog
                        fullWidth={this.state.fullWidth}
                        maxWidth={this.state.maxWidth}
                        open={this.state.open}
                        scroll={this.state.scroll}
                        aria-labelledby="scroll-dialog-title"
                        className={classes.modaladd}
                      >
                        <DialogTitle id="scroll-dialog-title">Formulir Persyaratan Perijinan Mendirikan Sekolah Baru</DialogTitle>
                        <DialogContent>
                          <Table>
                            <TableHead>
                              <TableRow>
                                <TableCell style={{ padding: 10, fontSize: '14px' }}>No</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">Persyaratan</TableCell>
                              </TableRow>
                            </TableHead>
                            <TableBody>
                              {/* 1 */}
                              <TableRow style={{ backgroundColor: '#f9f9f9' }}>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">1</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">	Surat permohonan izin kepada Gubernur/Administrator P2T</TableCell>
                                <TableCell align="right" style={{ minWidth:'130px' }}>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>
                              {/* 2 */}
                              <TableRow>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">2</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">	Pengajuan usulan 9 bulan sebelum Tahun Pelajaran dilengkapi hasil studi kelayakan dan diketahui oleh Kepala Desa/Lurah, Camat</TableCell>
                                <TableCell align="right">
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>
                              {/* 3 */}
                              <TableRow style={{ backgroundColor: '#f9f9f9' }}>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">3</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">	Sertifikat kepemilikan tanah dari notaris</TableCell>
                                <TableCell align="right">
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>
                              {/* 4 */}
                              <TableRow>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">4</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">	Akta notaris dari Kemenhukam</TableCell>
                                <TableCell align="right">
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>
                              {/* 5 */}
                              <TableRow style={{ backgroundColor: '#f9f9f9' }}>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">5</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">	Ijin Mendirikan Bangunan (IMB)</TableCell>
                                <TableCell align="right">
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>
                              {/* 6 */}
                              <TableRow>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">6</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">	Untuk SMK:</TableCell>
                              </TableRow>
                              {/* a */}
                              <TableRow style={{ backgroundColor: '#f9f9f9' }}>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">a)</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">1 Orang BK, TU dan Penjaga</TableCell>
                                <TableCell align="right">
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>
                              {/* b */}
                              <TableRow>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">b)</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">12 orang guru untuk setiap mata pelajaran</TableCell>
                                <TableCell align="right">
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>
                              {/* c */}
                              <TableRow style={{ backgroundColor: '#f9f9f9' }}>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">c)</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">2 orang guru untuk setiap program keahlian baru</TableCell>
                                <TableCell align="right">
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>
                              <TableRow>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">7</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">	Badan pengelolah berkewajiban menyediakan sarpras</TableCell>
                              </TableRow>
                              {/* a */}
                              <TableRow style={{ backgroundColor: '#f9f9f9' }}>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">a)</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">Lahan sekolah</TableCell>
                                <TableCell align="right">
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>
                              {/* b */}
                              <TableRow>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">b)</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">Ruang kelas</TableCell>
                                <TableCell align="right">
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>
                              {/* c */}
                              <TableRow style={{ backgroundColor: '#f9f9f9' }}>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">c)</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">Ruang kepala sekolah</TableCell>
                                <TableCell align="right">
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>
                              {/* d */}
                              <TableRow>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">d)</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">Ruang guru</TableCell>
                                <TableCell align="right">
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>
                              {/* e */}
                              <TableRow style={{ backgroundColor: '#f9f9f9' }}>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">e)</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">Ruang tata usaha</TableCell>
                                <TableCell align="right">
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>
                              {/* f */}
                              <TableRow>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">f)</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">Ruang ibadah</TableCell>
                                <TableCell align="right">
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>
                              {/* g */}
                              <TableRow style={{ backgroundColor: '#f9f9f9' }}>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">g)</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">Ruang perpustakaan</TableCell>
                                <TableCell align="right">
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>
                              {/* h */}
                              <TableRow>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">h)</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">Ruang laborat</TableCell>
                                <TableCell align="right">
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>
                              {/* i */}
                              <TableRow style={{ backgroundColor: '#f9f9f9' }}>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">i)</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">Ruang kantin </TableCell>
                                <TableCell align="right">
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>
                              {/* j */}
                              <TableRow>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">j)</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">Ruang instalasi dan daya</TableCell>
                                <TableCell align="right">
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>
                              {/* k */}
                              <TableRow style={{ backgroundColor: '#f9f9f9' }}>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">k)</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">Tempat olahraga</TableCell>
                                <TableCell align="right">
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>
                              {/* l */}
                              <TableRow>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">l)</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">Ruang UKS dan BP</TableCell>
                                <TableCell align="right">
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>
                              {/* m */}
                              <TableRow style={{ backgroundColor: '#f9f9f9' }}>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">m)</TableCell>
                                <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">Toilet untuk guru dan siswa</TableCell>
                                <TableCell align="right">
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#4caf50', color: '#FFFFFF' }} className={classes.button}>
                                    <InsertIcon className={classes.iconSmall} />
                                  </Button>
                                  <Button variant="contained" size="small" style={{ backgroundColor: '#fb483a', color: '#FFFFFF' }} className={classes.button}>
                                    <CheckIcon className={classes.iconSmall} />
                                  </Button>
                                </TableCell>
                              </TableRow>

                            </TableBody>
                          </Table>
                        </DialogContent>
                        <DialogActions>
                          <Button onClick={this.handleClose}>
                            SAVE CHANGES
                          </Button>
                          <Button onClick={this.handleClose}>
                            CANCEL
                          </Button>
                        </DialogActions>
                      </Dialog>
                    </div>

                    {/* TABLE */}
                    <div className={classes.tableWrapper}>
                      <Table className={classes.table}>
                        <TableHead>
                          <TableRow>
                            <CustomTableCell style={{ padding: 10 }} >#</CustomTableCell>
                            <CustomTableCell style={{ padding: 10 }} align="left">No Pegajuan</CustomTableCell>
                            <CustomTableCell style={{ padding: 10 }} align="left">Nama Yayasan</CustomTableCell>
                            <CustomTableCell style={{ padding: 10 }} align="center">Tanggal Pengajuan</CustomTableCell>
                            <CustomTableCell style={{ padding: 10 }} align="center">Status</CustomTableCell>
                            <CustomTableCell style={{ padding: 10 }} align="center">Aksi</CustomTableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {rows.slice(
                            page * rowsPerPage, 
                            page * rowsPerPage + rowsPerPage
                          ).map(row => (
                            <TableRow key={row.id}>
                              <TableCell style={{ padding: 10, fontSize: '14px' }} component="th" scope="row">{row.id} </TableCell>
                              <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">{row.nopengajuan}</TableCell>
                              <TableCell style={{ padding: 10, fontSize: '14px' }} align="left">{row.namayayasan}</TableCell>
                              <TableCell style={{ padding: 10, fontSize: '14px' }} align="center">{row.tanggalpengajuan}</TableCell>
                              <TableCell style={{ padding: 10, fontSize: '14px' }} align="center">
                                <span style={{ backgroundColor: '#03A9F4', color: '#FFFFFF', padding: '.2em .6em .3em', fontSize: '75%' }}>
                                  {row.status}
                                </span>
                              </TableCell>
                              <TableCell align="center">
                                <Button onClick={this.handleClickOpen('body')} variant="contained" size="small" style={{ backgroundColor: '#FF9800', color: '#FFFFFF' }} className={classes.button}>
                                  <Cached className={classes.iconSmall} />
                                </Button>
                                <Button variant="contained" size="small" style={{ backgroundColor: '#03A9F4', color: '#FFFFFF' }} className={classes.button}>
                                  <PrintIcon className={classes.iconSmall} />
                                </Button>
                                <Button variant="contained" size="small" style={{ backgroundColor: '#03A9F4', color: '#FFFFFF' }} className={classes.button}>
                                  <DeleteIcon className={classes.iconSmall} />
                                </Button>
                                <Button variant="contained" size="small" style={{ backgroundColor: '#4CAF50', color: '#FFFFFF' }} className={classes.button}>
                                  <EditIcon className={classes.iconSmall} />
                                </Button>
                                <Button variant="contained" size="small" color="secondary" className={classes.button}>
                                  <DeleteIcon className={classes.iconSmall} />
                                </Button>

                              </TableCell>
                            </TableRow>
                          ))}
                          {emptyRows > 0 && (
                            <TableRow style={{ height: 48 * emptyRows }}>
                              <TableCell colSpan={6} />
                            </TableRow>
                          )}
                        </TableBody>
                        <TableFooter>
                          <TableRow>
                            <TablePagination
                              rowsPerPageOptions={[5, 10, 25]}
                              colSpan={5}
                              count={rows.length}
                              rowsPerPage={rowsPerPage}
                              page={page}
                              onChangePage={this.handleChangePage}
                              onChangeRowsPerPage={this.handleChangeRowsPerPage}
                              ActionsComponent={TablePaginationActionsWrapped}
                            />
                          </TableRow>
                        </TableFooter>
                      </Table>
                    </div>
                  </Paper>
                </Grid>
              </Grid>

            </main>
          </div>
        </section>
      );
    }
  }
}

Component.propTypes = {
  classes: PropTypes.object,
  actions: PropTypes.object,
  isLoading: PropTypes.bool,
  data: PropTypes.array
};