import Developer from './Developer';
import Error404 from './Error404';
import Sekolah from './Sekolah';
import PendirianSekolah from './PendirianSekolah';
import ListOrderManagement from './ListOrderManagement';
import Login from './Login';

const pages = {
  Developer,
  Error404,
  Sekolah,
  PendirianSekolah,
  ListOrderManagement,
  Login,
};

export default pages;