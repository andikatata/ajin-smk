const images = {
  LOGO: '/assets/logo.png',
  SCHOOL: '/assets/school.png'
};

export default images;